﻿using RPG_Characters.Models;
using RPG_Characters.Models.Attributes;
using RPG_Characters.Models.Characters;
using RPG_Characters.Models.Items;
using System;

namespace RPG_Characters
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Warrior warrior = new() { Name = "Mr Warrior" };
            
            Armor testMailBody = new()
            {
                ItemName = "Common mail body armor",
                ItemLevel = 1,
                ItemSlot = Slot.Body,
                ArmorType = ArmorType.Mail,
                Attributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };

            Weapon testAxe = new() 
            { 
                ItemName = "Common axe",    
                ItemLevel = 1, 
                ItemSlot = Slot.Weapon,
                WeaponType = WeaponType.Axe, 
                WeaponAttributes = new(){ Damage = 7,AttackSpeed = 1.1} 
            };

            warrior.DisplayPlayerStats();
            Console.WriteLine();
            
            Console.WriteLine(warrior.EquipItem(testMailBody));

            warrior.DisplayPlayerStats();
            Console.WriteLine();
            
            Console.WriteLine(warrior.EquipItem(testAxe));
            warrior.DisplayPlayerStats();
        }
    }
}
