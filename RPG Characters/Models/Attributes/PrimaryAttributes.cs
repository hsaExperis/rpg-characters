﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Models
{
    public class PrimaryAttributes
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public int Vitality { get; set; }

        public PrimaryAttributes()
        {
            Strength = 0;
            Dexterity = 0;
            Intelligence = 0;
            Vitality = 0;
        }

        public static PrimaryAttributes operator +(PrimaryAttributes primaryAttributes1, PrimaryAttributes primaryAttributes2)
        {
            return new PrimaryAttributes()
            {
                Strength = primaryAttributes1.Strength + primaryAttributes2.Strength,
                Dexterity = primaryAttributes1.Dexterity + primaryAttributes2.Dexterity,
                Intelligence = primaryAttributes1.Intelligence + primaryAttributes2.Intelligence,
                Vitality = primaryAttributes1.Vitality + primaryAttributes2.Vitality
            };
        }
    }
}
