﻿using RPG_Characters.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Models.Items
{
    public class Weapon : Item
    {
        public WeaponType WeaponType { get; set; }
        public WeaponAttributes WeaponAttributes { get; set; }
        public double DPS
        {
            get { return WeaponAttributes.Damage * WeaponAttributes.AttackSpeed; }
        }
    }
}
