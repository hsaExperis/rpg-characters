﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Models
{
    public enum Slot
    {
        Head,
        Body,
        Legs,
        Weapon
    }

    public enum WeaponType
    {
        Axe,
        Bow,
        Dagger,
        Hammer,
        Staff,
        Sword,
        Wand
    }

    public enum ArmorType
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }
}
