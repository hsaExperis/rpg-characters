﻿using RPG_Characters.Exceptions;
using RPG_Characters.Models.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Models.Characters
{
    public class Mage : Character
    {
        public Mage()
        {
            AllowedWeapons = new List<WeaponType> { WeaponType.Staff, WeaponType.Wand };
            AllowedArmor = new List<ArmorType> { ArmorType.Cloth };
            BasePrimaryAttributes = new PrimaryAttributes() { Vitality = 5, Strength = 1, Dexterity = 1, Intelligence = 8 };
        }

        public override double DPS 
        { 
            get
            {
                return CalculateDPS(TotalPrimaryAttributes.Intelligence);
            }
        }

        protected override void AddLevelUpStats()
        {
            BasePrimaryAttributes += new PrimaryAttributes() { Vitality = 3, Strength = 1, Dexterity = 1, Intelligence = 5 };
        }
    }
}
