﻿using RPG_Characters.Models.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Models.Characters
{
    public class Warrior : Character
    {
        public Warrior()
        {
            AllowedWeapons = new List<WeaponType> { WeaponType.Axe, WeaponType.Hammer, WeaponType.Sword };
            AllowedArmor = new List<ArmorType> { ArmorType.Mail, ArmorType.Plate };
            BasePrimaryAttributes = new PrimaryAttributes() { Vitality = 10, Strength = 5, Dexterity = 2, Intelligence = 1 };
        }

        public override double DPS
        {
            get
            {
                return CalculateDPS(TotalPrimaryAttributes.Strength);
            }
        }

        protected override void AddLevelUpStats()
        {
            BasePrimaryAttributes += new PrimaryAttributes() { Vitality = 5, Strength = 3, Dexterity = 2, Intelligence = 1 };
        }
    }
}
