﻿using RPG_Characters.Exceptions;
using RPG_Characters.Models.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Models
{
    public abstract class Character
    {
        private PrimaryAttributes _primaryAttributes;
        private SecondaryAttributes _secondaryAttributes;

        public string Name { get; set; }
        public int Level { get; set; }
        public abstract double DPS { get; }
        public PrimaryAttributes BasePrimaryAttributes { get; set; }
        public PrimaryAttributes TotalPrimaryAttributes 
        { 
            get 
            {
                _primaryAttributes = BasePrimaryAttributes;
                foreach (Item item in Equipment.Values)
                {
                    if (item is Armor)
                    {
                        Armor armor = item as Armor;
                        _primaryAttributes += armor.Attributes;
                    }
                }
                return _primaryAttributes;
            }
        }
        public SecondaryAttributes SecondaryAttributes {
            get
            {
                _secondaryAttributes.Health = TotalPrimaryAttributes.Vitality * 10;
                _secondaryAttributes.ArmorRating = TotalPrimaryAttributes.Strength + TotalPrimaryAttributes.Dexterity;
                _secondaryAttributes.ElementalResistance = TotalPrimaryAttributes.Intelligence;
                return _secondaryAttributes;
            }
            set 
            {
                _secondaryAttributes = value;
            }
        }
        protected Dictionary<Slot, Item> Equipment { get; set; }
        protected List<WeaponType> AllowedWeapons { get; set; }
        protected List<ArmorType> AllowedArmor { get; set; }

        public Character()
        {
            Level = 1;

            Equipment = new();
            SecondaryAttributes = new();
        }

        protected abstract void AddLevelUpStats();

        protected double CalculateDPS(int attribute)
        {
            if (Equipment.ContainsKey(Slot.Weapon))
            {
                Weapon weapon = Equipment[Slot.Weapon] as Weapon;
                return weapon.DPS * (1 + attribute / 100);
            }
            else
            {
                return 1;
            }
        }

        private string EquipWeapon(Weapon weapon)
        {
            if (AllowedWeapons.Contains(weapon.WeaponType))
            {
                Equipment.Add(weapon.ItemSlot, weapon);
                return "New weapon equipped!";
            } 
            else
            {
                throw new InvalidWeaponException("Weapon not of the correct type");
            }
        }

        private string EquipArmor(Armor armor)
        {
            if (AllowedArmor.Contains(armor.ArmorType))
            {
                Equipment.Add(armor.ItemSlot, armor);
                return "New armor equipped!";
            } 
            else
            {
                throw new InvalidArmorException("Armor not of the correct type");
            }
        }

        /// <summary>
        /// Equips an item, either a weapon or armor. Throws InvalidWeaponException or InvalidArmorException
        /// </summary>
        /// <param name="item">Item to be equipped, weapon or armor</param>
        /// <returns>string, message with information on the result of the equip</returns>
        public string EquipItem(Item item)
        {
            if (item is Weapon && item.ItemLevel <= Level)
            {
                return EquipWeapon(item as Weapon);
            }
            else if (item is Weapon)
            {
                throw new InvalidWeaponException("Weapon level is too high");
            }
            else if (item is Armor && item.ItemLevel <= Level)
            {
                return EquipArmor(item as Armor);
            }
            else if (item is Armor)
            {
                throw new InvalidArmorException("Armor level is too high");
            } 
            else
            {
                return "Not successfully equipped";
            }
        }
        /// <summary>
        /// Levels up the character and adds stats according to the type of hero
        /// </summary>
        /// <param name="level">The amount that the level should increase with</param>
        public void LevelUp(int level)
        {
            if (level > 0)
            {
                Level += level;

                for (int i = 0; i < level; i++)
                {
                    AddLevelUpStats();
                }
            }
            else
            {
                throw new ArgumentException("Input parameter must be bigger than 0");
            }
        }

        /// <summary>
        /// Displays the stats of the character in the console
        /// </summary>
        public void DisplayPlayerStats()
        {
            StringBuilder stringBuilder = new();
            stringBuilder.Append($"Name: {Name}");
            stringBuilder.AppendLine();
            stringBuilder.Append($"Level: {Level}");
            stringBuilder.AppendLine();
            stringBuilder.Append($"Strength: {TotalPrimaryAttributes.Strength}");
            stringBuilder.AppendLine();
            stringBuilder.Append($"Dexterity: {TotalPrimaryAttributes.Dexterity}");
            stringBuilder.AppendLine();
            stringBuilder.Append($"Intelligence: {TotalPrimaryAttributes.Intelligence}");
            stringBuilder.AppendLine();
            stringBuilder.Append($"Health: {SecondaryAttributes.Health}");
            stringBuilder.AppendLine();
            stringBuilder.Append($"Armor Rating: {SecondaryAttributes.ArmorRating}");
            stringBuilder.AppendLine();
            stringBuilder.Append($"Elemental Resistance: {SecondaryAttributes.ElementalResistance}");
            stringBuilder.AppendLine();
            stringBuilder.Append($"DPS: {DPS:0.##}");

            Console.WriteLine(stringBuilder);
        }
    }
}
