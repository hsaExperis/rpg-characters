﻿using RPG_Characters.Models.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Models.Characters
{
    public class Ranger : Character
    {
        public Ranger()
        {
            AllowedWeapons = new List<WeaponType> { WeaponType.Bow };
            AllowedArmor = new List<ArmorType> { ArmorType.Leather, ArmorType.Mail };
            BasePrimaryAttributes = new PrimaryAttributes() { Vitality = 8, Strength = 1, Dexterity = 7, Intelligence = 1 };
        }

        public override double DPS
        {
            get
            {
                return CalculateDPS(TotalPrimaryAttributes.Dexterity);
            }
        }

        protected override void AddLevelUpStats()
        {
            BasePrimaryAttributes += new PrimaryAttributes() { Vitality = 2, Strength = 1, Dexterity = 5, Intelligence = 1 };
        }
    }
}
