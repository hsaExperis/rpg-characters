﻿using RPG_Characters.Models.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Models.Characters
{
    public class Rogue : Character
    {
        public Rogue()
        {
            AllowedWeapons = new List<WeaponType> { WeaponType.Dagger, WeaponType.Sword };
            AllowedArmor = new List<ArmorType> { ArmorType.Leather, ArmorType.Mail };
            BasePrimaryAttributes = new PrimaryAttributes() { Vitality = 8, Strength = 2, Dexterity = 6, Intelligence = 1 };
        }

        public override double DPS
        {
            get
            {
                return CalculateDPS(TotalPrimaryAttributes.Dexterity);
            }
        }

        protected override void AddLevelUpStats()
        {
            BasePrimaryAttributes += new PrimaryAttributes() { Vitality = 3, Strength = 1, Dexterity = 4, Intelligence =  1};
        }
    }
}
