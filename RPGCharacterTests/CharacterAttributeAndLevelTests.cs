﻿using RPG_Characters.Models.Characters;
using System;
using System.Collections.Generic;
using Xunit;

namespace RPGCharacterTests
{
    public class CharacterAttributeAndLevelTests
    {
        [Fact]
        public void CharacterConstructor_CreatingMage_IsLevelOne()
        {
            // Arrange and act
            int expected = 1;
            Mage mage = new();

            // Assert
            Assert.Equal(expected, mage.Level);
        }

        [Fact]
        public void LevelUp_LevelUpMage_IsLevelTwo()
        {
            // Arrange
            int expected = 2;
            Mage mage = new();

            // Act
            mage.LevelUp(1);

            // Assert
            Assert.Equal(expected, mage.Level);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public void LevelUp_LevelDownMage_ArgumentException(int level)
        {
            // Arrange
            Mage mage = new();

            // Act and assert
            Assert.Throws<ArgumentException>(() => mage.LevelUp(level));
        }

        // Create all characters
        [Fact]
        public void MageConstructor_CreateCharacter_CorrectDeafultValues()
        {
            // Arrange and act
            Mage character = new();

            List<int> attributes = new()
            {
                character.TotalPrimaryAttributes.Vitality,
                character.TotalPrimaryAttributes.Strength,
                character.TotalPrimaryAttributes.Dexterity,
                character.TotalPrimaryAttributes.Intelligence
            };

            List<int> expectedAttributes = new() { 5, 1, 1, 8 };

            // Assert
            Assert.Equal(expectedAttributes, attributes);
        }

        [Fact]
        public void RangerConstructor_CreateCharacter_CorrectDeafultValues()
        {
            // Arrange and act
            Ranger character = new();

            List<int> attributes = new()
            {
                character.TotalPrimaryAttributes.Vitality,
                character.TotalPrimaryAttributes.Strength,
                character.TotalPrimaryAttributes.Dexterity,
                character.TotalPrimaryAttributes.Intelligence
            };

            List<int> expectedAttributes = new() { 8, 1, 7, 1 };

            // Assert
            Assert.Equal(expectedAttributes, attributes);
        }

        [Fact]
        public void RogueConstructor_CreateCharacter_CorrectDeafultValues()
        {
            // Arrange and act
            Rogue character = new();

            List<int> attributes = new()
            {
                character.TotalPrimaryAttributes.Vitality,
                character.TotalPrimaryAttributes.Strength,
                character.TotalPrimaryAttributes.Dexterity,
                character.TotalPrimaryAttributes.Intelligence
            };

            List<int> expectedAttributes = new() { 8, 2, 6, 1 };

            // Assert
            Assert.Equal(expectedAttributes, attributes);
        }

        [Fact]
        public void WarriorConstructor_CreateCharacter_CorrectDeafultValues()
        {
            // Arrange and act
            Warrior character = new();

            List<int> attributes = new()
            {
                character.TotalPrimaryAttributes.Vitality,
                character.TotalPrimaryAttributes.Strength,
                character.TotalPrimaryAttributes.Dexterity,
                character.TotalPrimaryAttributes.Intelligence
            };

            List<int> expectedAttributes = new() { 10, 5, 2, 1 };

            // Assert
            Assert.Equal(expectedAttributes, attributes);
        }

        // Level up all characters
        [Fact]
        public void LevelUp_MageOneLevelUp_CorrectDeafultValues()
        {
            // Arrange and act
            Mage character = new();
            character.LevelUp(1);

            List<int> attributes = new()
            {
                character.TotalPrimaryAttributes.Vitality,
                character.TotalPrimaryAttributes.Strength,
                character.TotalPrimaryAttributes.Dexterity,
                character.TotalPrimaryAttributes.Intelligence
            };

            List<int> expectedAttributes = new() { 8, 2, 2, 13 };

            // Assert
            Assert.Equal(expectedAttributes, attributes);
        }

        [Fact]
        public void LevelUp_RangerOneLevelUp_CorrectDeafultValues()
        {
            // Arrange and act
            Ranger character = new();
            character.LevelUp(1);

            List<int> attributes = new()
            {
                character.TotalPrimaryAttributes.Vitality,
                character.TotalPrimaryAttributes.Strength,
                character.TotalPrimaryAttributes.Dexterity,
                character.TotalPrimaryAttributes.Intelligence
            };

            List<int> expectedAttributes = new() { 10, 2, 12, 2 };

            // Assert
            Assert.Equal(expectedAttributes, attributes);
        }

        [Fact]
        public void LevelUp_RogueOneLevelUp_CorrectDeafultValues()
        {
            // Arrange and act
            Rogue character = new();
            character.LevelUp(1);

            List<int> attributes = new()
            {
                character.TotalPrimaryAttributes.Vitality,
                character.TotalPrimaryAttributes.Strength,
                character.TotalPrimaryAttributes.Dexterity,
                character.TotalPrimaryAttributes.Intelligence
            };

            List<int> expectedAttributes = new() { 11, 3, 10, 2 };

            // Assert
            Assert.Equal(expectedAttributes, attributes);
        }

        [Fact]
        public void LevelUp_WarriorOneLevelUp_CorrectDeafultValues()
        {
            // Arrange and act
            Warrior character = new();
            character.LevelUp(1);

            List<int> attributes = new()
            {
                character.TotalPrimaryAttributes.Vitality,
                character.TotalPrimaryAttributes.Strength,
                character.TotalPrimaryAttributes.Dexterity,
                character.TotalPrimaryAttributes.Intelligence
            };

            List<int> expectedAttributes = new() { 15, 8, 4, 2 };

            // Assert
            Assert.Equal(expectedAttributes, attributes);
        }

        [Fact]
        public void LevelUp_LevelingUpWarrior_CorrectSecondaryValues()
        {
            // Arrange and act
            Warrior warrior = new();
            warrior.LevelUp(1);

            List<int> attributes = new()
            {
                warrior.SecondaryAttributes.Health,
                warrior.SecondaryAttributes.ArmorRating,
                warrior.SecondaryAttributes.ElementalResistance
            };

            List<int> expectedAttributes = new() { 150, 12, 2 };

            // Assert
            Assert.Equal(expectedAttributes, attributes);
        }
    }
}
