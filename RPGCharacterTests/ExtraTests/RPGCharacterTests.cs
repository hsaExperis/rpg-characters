﻿using System;
using RPG_Characters.Models;
using RPG_Characters.Models.Characters;
using RPG_Characters.Models.Items;
using Xunit;

namespace RPGCharacterTests
{
    public class RPGCharacterTests
    {
        [Fact]
        public void EquipItem_EquipMail_CorrectTotalAttributes()
        {
            // Arrange
            int expected = 10;
            Rogue rogue = new();

            Armor testMailBody = new() 
            { 
                ItemName = "Common mail body armor", 
                ItemLevel = 1, 
                ItemSlot = Slot.Body, 
                ArmorType = ArmorType.Mail, 
                Attributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 } 
            };

            //Act
            rogue.EquipItem(testMailBody);
            int actual = rogue.TotalPrimaryAttributes.Vitality;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipItem_WeaponEquipped_CorrectDPSValue()
        {
            // Arrange
            Warrior warrior = new();

            Weapon testAxe = new() 
            { 
                ItemName = "Common axe",    
                ItemLevel = 1, 
                ItemSlot = Slot.Weapon,
                WeaponType = WeaponType.Axe, 
                WeaponAttributes = new(){ Damage = 7,AttackSpeed = 1.1} 
            };

            double expected = testAxe.DPS * (1 + warrior.TotalPrimaryAttributes.Strength / 100);

            // Act
            warrior.EquipItem(testAxe);

            // Assert
            Assert.Equal(expected, warrior.DPS);
        }

        [Fact]
        public void EquipItem_ArmorEquipped_CorrectHealthValue()
        {
            // Arrange
            Warrior warrior = new();

            Armor testMailBody = new()
            {
                ItemName = "Common mail body armor",
                ItemLevel = 1,
                ItemSlot = Slot.Body,
                ArmorType = ArmorType.Mail,
                Attributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };

            double expected = (warrior.TotalPrimaryAttributes.Vitality + testMailBody.Attributes.Vitality) * 10;

            // Act
            warrior.EquipItem(testMailBody);

            // Assert
            Assert.Equal(expected, warrior.SecondaryAttributes.Health);
        }
    }
}
