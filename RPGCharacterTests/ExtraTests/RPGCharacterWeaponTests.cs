using System;
using Xunit;
using RPG_Characters.Models.Items;
using RPG_Characters.Models;
using RPG_Characters.Models.Attributes;
using RPG_Characters.Models.Characters;
using RPG_Characters.Exceptions;

namespace RPGCharacterTests
{
    public class RPGCharacterWeaponTests
    {
        [Fact]
        public void EquipItem_WeaponLevelToHigh_InvalidWeaponException()
        {
            // Arrange
            Mage mage = new();

            Weapon staff = new()
            {
                ItemName = "Common staff",
                ItemLevel = 2,
                ItemSlot = Slot.Weapon,
                WeaponType = WeaponType.Staff,
                WeaponAttributes = new() { Damage = 12, AttackSpeed = 0.8 }
            };

            // Act and assert
            Assert.Throws<InvalidWeaponException> (() => mage.EquipItem(staff));
        }

        [Fact]
        public void EquipItem_WeaponNotAllowed_InvalidWeaponException()
        {
            // Arrange
            Mage mage = new();

            Weapon bow = new()
            {
                ItemName = "Common bow",
                ItemLevel = 1,
                ItemSlot = Slot.Weapon,
                WeaponType = WeaponType.Bow,
                WeaponAttributes = new() { Damage = 12, AttackSpeed = 0.8 }
            };

            // Act and assert
            Assert.Throws<InvalidWeaponException>(() => mage.EquipItem(bow));
        }
    }
}
