﻿using RPG_Characters.Exceptions;
using RPG_Characters.Models;
using RPG_Characters.Models.Characters;
using RPG_Characters.Models.Items;
using System;
using Xunit;

namespace RPGCharacterTests
{
    public class RPGCharacterArmorTests
    {
        [Fact]
        public void EquipItem_ArmorLevelTooHigh_InvalidArmorException()
        {
            // Arrange
            Warrior warrior = new();

            Armor testPlateBody = new() 
            { 
                ItemName = "Common plate   body armor", 
                ItemLevel = 2, 
                ItemSlot = Slot.Body, 
                ArmorType = ArmorType.Plate, 
                Attributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 } 
            };

            Assert.Throws<InvalidArmorException>(() => warrior.EquipItem(testPlateBody));
        }

        [Fact]
        public void EquipItem_ArmorNotAllowed_InvalidArmorException()
        {
            // Arrange
            Rogue rogue = new();

            Armor testPlateBody = new()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = Slot.Body,
                ArmorType = ArmorType.Plate,
                Attributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };

            Assert.Throws<InvalidArmorException>(() => rogue.EquipItem(testPlateBody));
        }

    }
}
