﻿using RPG_Characters.Exceptions;
using RPG_Characters.Models;
using RPG_Characters.Models.Characters;
using RPG_Characters.Models.Items;
using System;
using Xunit;

namespace RPGCharacterTests
{
    public class ItemsAndEquipmentTests
    {
        [Fact]
        public void EquipItem_WeaponLevelToHigh_InvalidWeaponException()
        {
            // Arrange
            Warrior warrior = new();

            Weapon testAxe = new() 
            { 
                ItemName = "Common axe",    
                ItemLevel = 2, 
                ItemSlot = Slot.Weapon,
                WeaponType = WeaponType.Axe, 
                WeaponAttributes = new(){ Damage = 7, AttackSpeed = 1.1} 
            };

            // Act and assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipItem(testAxe));
        }

        [Fact]
        public void EquipItem_ArmorLevelTooHigh_InvalidArmorException()
        {
            // Arrange
            Warrior warrior = new();

            Armor testPlateBody = new()
            {
                ItemName = "Common plate   body armor",
                ItemLevel = 2,
                ItemSlot = Slot.Body,
                ArmorType = ArmorType.Plate,
                Attributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };

            Assert.Throws<InvalidArmorException>(() => warrior.EquipItem(testPlateBody));
        }

        [Fact]
        public void EquipItem_WeaponNotAllowed_InvalidWeaponException()
        {
            // Arrange
            Warrior warrior = new();

            Weapon bow = new()
            {
                ItemName = "Common bow",
                ItemLevel = 1,
                ItemSlot = Slot.Weapon,
                WeaponType = WeaponType.Bow,
                WeaponAttributes = new() { Damage = 12, AttackSpeed = 0.8 }
            };

            // Act and assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipItem(bow));
        }

        [Fact]
        public void EquipItem_ArmorNotAllowed_InvalidArmorException()
        {
            // Arrange
            Warrior warrior = new();

            Armor testClothHead = new() 
            { 
                ItemName = "Common cloth head armor", 
                ItemLevel = 1, ItemSlot = Slot.Head, 
                ArmorType = ArmorType.Cloth, 
                Attributes = new () { Vitality = 1, Intelligence = 5 } };

            Assert.Throws<InvalidArmorException>(() => warrior.EquipItem(testClothHead));
        }

        [Fact]
        public void EquipItem_WeaponAllowed_WeaponSuccessfullyEquipped()
        {
            // Arrange
            string expected = "New weapon equipped!";
            Warrior warrior = new();

            Weapon testAxe = new()
            {
                ItemName = "Common axe",
                ItemLevel = 1,
                ItemSlot = Slot.Weapon,
                WeaponType = WeaponType.Axe,
                WeaponAttributes = new() { Damage = 7, AttackSpeed = 1.1 }
            };

            // Act 
            string message = warrior.EquipItem(testAxe);

            // Assert
            Assert.Equal(expected, message);
        }

        [Fact]
        public void EquipItem_ArmorAllowed_ArmorSuccessfullyEquipped()
        {
            // Arrange
            string expected = "New armor equipped!";
            Warrior warrior = new();

            Armor testPlateBody = new()
            {
                ItemName = "Common plate   body armor",
                ItemLevel = 1,
                ItemSlot = Slot.Body,
                ArmorType = ArmorType.Plate,
                Attributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };

            // Act 
            string message = warrior.EquipItem(testPlateBody);

            // Assert
            Assert.Equal(expected, message);
        }

        [Fact]
        public void CalculateDPS_NoWeaponEquipped_MeleeDPS()
        {
            // Arrange
            double expected = 1 * (1 + (5/100));
            Warrior warrior = new();

            // Act
            double actual = warrior.DPS;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDPS_WeaponEquipped_WeaponDPS()
        {
            // Arrange
            double expected = (7 * 1.1) * (1 + (5 / 100));
            Warrior warrior = new();

            Weapon testAxe = new()
            {
                ItemName = "Common axe",
                ItemLevel = 1,
                ItemSlot = Slot.Weapon,
                WeaponType = WeaponType.Axe,
                WeaponAttributes = new() { Damage = 7, AttackSpeed = 1.1 }
            };

            // Act
            warrior.EquipItem(testAxe);
            double actual = warrior.DPS;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDPS_WeaponAndArmorEquipped_WeaponAndArmorDPS()
        {
            // Arrange
            double expected = (7 * 1.1) * (1 + ((5 + 1) / 100));
            Warrior warrior = new();

            Weapon testAxe = new()
            {
                ItemName = "Common axe",
                ItemLevel = 1,
                ItemSlot = Slot.Weapon,
                WeaponType = WeaponType.Axe,
                WeaponAttributes = new() { Damage = 7, AttackSpeed = 1.1 }
            };

            Armor testPlateBody = new()
            {
                ItemName = "Common plate   body armor",
                ItemLevel = 1,
                ItemSlot = Slot.Body,
                ArmorType = ArmorType.Plate,
                Attributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };

            // Act
            warrior.EquipItem(testAxe);
            warrior.EquipItem(testPlateBody);
            double actual = warrior.DPS;

            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
